# Database Managed by Liquibase

For this project, you will make your changes as a series of changesets with Liquibase to manage the demo database schema.

## Install Liquibase locally

* <https://www.liquibase.org/download>

## Install the JDBC Driver

* <https://jdbc.postgresql.org/download.html>

## Running local databases

When you're ready to start the project run:

```bash
#!/bin/bash
cp .env.example .env
docker-compose up
```

The docker-compose.yml file does not define any volumes so the data will not persist between sessions.

* You can update the .env file for you're values or leave it as is. The .env file is in the .gitignore so it will not be tracked in git.

## Using Liquibase

Liquibase is an open-source database schema change management solution which enables you to manage revisions of your database changes easily. Learn more about Liquibase here: <https://www.liquibase.org>

For this project, you will make your changes as a series of changesets with Liquibase.

When you're ready to work with the project you will run `docker-compose up` from the root of the project. When the databases come up you will be able to access them using the connection details in the .env.example file.

You will write a changeset as a sql statement and save it in the changelog/updates directory. The schema directory houses the base schema of the database as a series of changesets. The .xml file represents all of the changes you would like to include in your changeset and the changelog.xml file brings all of it together.

### Promoting changesets to demo_dev_environments database container

When you have your changeset ready you can apply it to the demo_dev_environments database by running this command from the liquibase directory.

```bash
#!/bin/bash
liquibase update
```

### Promoting changesets to demo_test_environments database container

When you have your changeset ready you can apply it to the demo_test_environments database by running this command from the liquibase directory.

```bash
#!/bin/bash
./scripts/migrate.sh
```
