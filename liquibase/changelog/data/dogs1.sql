-- liquibase formatted sql
-- changeset jgm:demoData
-- ----------------------------
-- Records of dogs1
-- ----------------------------
INSERT INTO "public"."dogs" ("breed") VALUES ('Affenpinscher');
INSERT INTO "public"."dogs" ("breed") VALUES ('Afghan Hound');
INSERT INTO "public"."dogs" ("breed") VALUES ('Airedale Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('Akita');
INSERT INTO "public"."dogs" ("breed") VALUES ('Alaskan Klee Kai');
INSERT INTO "public"."dogs" ("breed") VALUES ('Alaskan Malamute');
INSERT INTO "public"."dogs" ("breed") VALUES ('American Bulldog');
INSERT INTO "public"."dogs" ("breed") VALUES ('American English Coonhound');
INSERT INTO "public"."dogs" ("breed") VALUES ('American Eskimo Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('American Foxhound');
INSERT INTO "public"."dogs" ("breed") VALUES ('American Hairless Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('American Leopard Hound');
INSERT INTO "public"."dogs" ("breed") VALUES ('American Staffordshire Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('American Water Spaniel');
INSERT INTO "public"."dogs" ("breed") VALUES ('Anatolian Shepherd Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Appenzeller Sennenhund');
INSERT INTO "public"."dogs" ("breed") VALUES ('Australian Cattle Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Australian Kelpie');
INSERT INTO "public"."dogs" ("breed") VALUES ('Australian Shepherd');
INSERT INTO "public"."dogs" ("breed") VALUES ('Australian Stumpy Tail Cattle Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Australian Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('Azawakh');
INSERT INTO "public"."dogs" ("breed") VALUES ('Barbado da Terceira');
INSERT INTO "public"."dogs" ("breed") VALUES ('Barbet');
INSERT INTO "public"."dogs" ("breed") VALUES ('Basenji');
INSERT INTO "public"."dogs" ("breed") VALUES ('Basset Fauve de Bretagne');
INSERT INTO "public"."dogs" ("breed") VALUES ('Basset Hound');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bavarian Mountain Scent Hound');
INSERT INTO "public"."dogs" ("breed") VALUES ('Beagle');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bearded Collie');
INSERT INTO "public"."dogs" ("breed") VALUES ('Beauceron');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bedlington Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('Belgian Laekenois');
INSERT INTO "public"."dogs" ("breed") VALUES ('Belgian Malinois');
INSERT INTO "public"."dogs" ("breed") VALUES ('Belgian Sheepdog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Belgian Tervuren');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bergamasco Sheepdog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Berger Picard');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bernese Mountain Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bichon Frise');
INSERT INTO "public"."dogs" ("breed") VALUES ('Biewer Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('Black and Tan Coonhound');
INSERT INTO "public"."dogs" ("breed") VALUES ('Black Russian Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bloodhound');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bluetick Coonhound');
INSERT INTO "public"."dogs" ("breed") VALUES ('Boerboel');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bohemian Shepherd');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bolognese');
INSERT INTO "public"."dogs" ("breed") VALUES ('Border Collie');
INSERT INTO "public"."dogs" ("breed") VALUES ('Border Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('Borzoi');
INSERT INTO "public"."dogs" ("breed") VALUES ('Boston Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bouvier des Flandres');
INSERT INTO "public"."dogs" ("breed") VALUES ('Boxer');
INSERT INTO "public"."dogs" ("breed") VALUES ('Boykin Spaniel');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bracco Italiano');
INSERT INTO "public"."dogs" ("breed") VALUES ('Braque du Bourbonnais');
INSERT INTO "public"."dogs" ("breed") VALUES ('Braque Francais Pyrenean');
INSERT INTO "public"."dogs" ("breed") VALUES ('Briard');
INSERT INTO "public"."dogs" ("breed") VALUES ('Brittany');
INSERT INTO "public"."dogs" ("breed") VALUES ('Broholmer');
INSERT INTO "public"."dogs" ("breed") VALUES ('Brussels Griffon');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bull Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bulldog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Bullmastiff');
INSERT INTO "public"."dogs" ("breed") VALUES ('Cairn Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('Canaan Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Cane Corso');
INSERT INTO "public"."dogs" ("breed") VALUES ('Cardigan Welsh Corgi');
INSERT INTO "public"."dogs" ("breed") VALUES ('Carolina Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Catahoula Leopard Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Caucasian Shepherd Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Cavalier King Charles Spaniel');
INSERT INTO "public"."dogs" ("breed") VALUES ('Central Asian Shepherd Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Cesky Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('Chesapeake Bay Retriever');
INSERT INTO "public"."dogs" ("breed") VALUES ('Chihuahua');
INSERT INTO "public"."dogs" ("breed") VALUES ('Chinese Crested');
INSERT INTO "public"."dogs" ("breed") VALUES ('Chinese Shar-Pei');
INSERT INTO "public"."dogs" ("breed") VALUES ('Chinook');
INSERT INTO "public"."dogs" ("breed") VALUES ('Chow Chow');
INSERT INTO "public"."dogs" ("breed") VALUES ('Cirneco dell’Etna');
INSERT INTO "public"."dogs" ("breed") VALUES ('Clumber Spaniel');
INSERT INTO "public"."dogs" ("breed") VALUES ('Cocker Spaniel');
INSERT INTO "public"."dogs" ("breed") VALUES ('Collie');
INSERT INTO "public"."dogs" ("breed") VALUES ('Coton de Tulear');
INSERT INTO "public"."dogs" ("breed") VALUES ('Croatian Sheepdog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Curly-Coated Retriever');
INSERT INTO "public"."dogs" ("breed") VALUES ('Czechoslovakian Vlcak');
INSERT INTO "public"."dogs" ("breed") VALUES ('Dachshund');
INSERT INTO "public"."dogs" ("breed") VALUES ('Dalmatian');
INSERT INTO "public"."dogs" ("breed") VALUES ('Dandie Dinmont Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('Danish-Swedish Farmdog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Deutscher Wachtelhund');
INSERT INTO "public"."dogs" ("breed") VALUES ('Doberman Pinscher');
INSERT INTO "public"."dogs" ("breed") VALUES ('Dogo Argentino');
INSERT INTO "public"."dogs" ("breed") VALUES ('Dogue de Bordeaux');
INSERT INTO "public"."dogs" ("breed") VALUES ('Drentsche Patrijshond');
INSERT INTO "public"."dogs" ("breed") VALUES ('Drever');
INSERT INTO "public"."dogs" ("breed") VALUES ('Dutch Shepherd');
INSERT INTO "public"."dogs" ("breed") VALUES ('English Cocker Spaniel');
INSERT INTO "public"."dogs" ("breed") VALUES ('English Foxhound');
INSERT INTO "public"."dogs" ("breed") VALUES ('English Setter');
INSERT INTO "public"."dogs" ("breed") VALUES ('English Springer Spaniel');
INSERT INTO "public"."dogs" ("breed") VALUES ('English Toy Spaniel');
INSERT INTO "public"."dogs" ("breed") VALUES ('Entlebucher Mountain Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Estrela Mountain Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Eurasier');
INSERT INTO "public"."dogs" ("breed") VALUES ('Field Spaniel');
INSERT INTO "public"."dogs" ("breed") VALUES ('Finnish Lapphund');
INSERT INTO "public"."dogs" ("breed") VALUES ('Finnish Spitz');
INSERT INTO "public"."dogs" ("breed") VALUES ('Flat-Coated Retriever');
INSERT INTO "public"."dogs" ("breed") VALUES ('French Bulldog');
INSERT INTO "public"."dogs" ("breed") VALUES ('French Spaniel');
INSERT INTO "public"."dogs" ("breed") VALUES ('German Longhaired Pointer');
INSERT INTO "public"."dogs" ("breed") VALUES ('German Pinscher');
INSERT INTO "public"."dogs" ("breed") VALUES ('German Shepherd Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('German Shorthaired Pointer');
INSERT INTO "public"."dogs" ("breed") VALUES ('German Spitz');
INSERT INTO "public"."dogs" ("breed") VALUES ('German Wirehaired Pointer');
INSERT INTO "public"."dogs" ("breed") VALUES ('Giant Schnauzer');
INSERT INTO "public"."dogs" ("breed") VALUES ('Glen of Imaal Terrier');
INSERT INTO "public"."dogs" ("breed") VALUES ('Golden Retriever');
INSERT INTO "public"."dogs" ("breed") VALUES ('Gordon Setter');
INSERT INTO "public"."dogs" ("breed") VALUES ('Grand Basset Griffon Vendéen');
INSERT INTO "public"."dogs" ("breed") VALUES ('Great Dane');
INSERT INTO "public"."dogs" ("breed") VALUES ('Great Pyrenees');
INSERT INTO "public"."dogs" ("breed") VALUES ('Greater Swiss Mountain Dog');
INSERT INTO "public"."dogs" ("breed") VALUES ('Greyhound');
