-- liquibase formatted sql
-- changeset jgm:demoSchema
-- ----------------------------
-- Table structure for dogs
-- ----------------------------
DROP TABLE IF EXISTS "public"."dogs";
CREATE TABLE "public"."dogs" (
  "id" serial PRIMARY KEY,
  "breed" varchar(75)
);

ALTER TABLE "public"."dogs" OWNER TO "demo";
